public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());
        System.out.println(dog.avgBreedWeight());
        Labrador labra = new Labrador("frost");
        System.out.println(labra.getName() + " says " + labra.speak());
        System.out.println(labra.avgBreedWeight());//error if the weight isn't a static integer
        Yorkshire york= new Yorkshire("belle");
        System.out.println(york.getName() + " says " + york.speak());
        System.out.println(york.avgBreedWeight());//error if the weight isn't a static integer
    }
}
